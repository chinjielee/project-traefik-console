# Reverse Proxy > Traefikd
## Traefik global on off for windows

This Traefik project is to be used for all marketplace related projects

Add local domain **tk.local** to your hosts file

<a href="https://ecompile.io/blog/localhost-custom-domain-name" > How to setup Localhost custom domain </a>

For Mac and Linux

**/etc/hosts**  

For windows 

**c:\Windows\System32\Drivers\etc\hosts**

## For First Time Setup

Execute init.sh 


## To enable and disable traefik

For Mac and Linux

Execute on.sh or off.sh 

For Windows

Execute on.bat or off.bat 

## 

Once executed the on.sh/on.bat , you can should be able to access it in your web browser via <a href="
 http://tk.local/dashboard/#/"> http://tk.local/dashboard/#/ </a>


There is default basic auth 

Default Username : test  
Default Password : test


 
